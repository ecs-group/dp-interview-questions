### A simple coding test to build a simple Spring Boot endpoint.

We have 3 initially failing tests which you can see by running the following command

```
mvn clean test
```

Look at SpringController.java to see what methods to add.
Then get the tests to pass.

[SpringController.java](./src/main/java/com/ecs/springboot/SpringController.java)