package com.ecs.springboot;

import org.springframework.web.bind.annotation.*;

/**
 * Add a simple rest endpoint
 * The endpoint takes a single request parameter 'message' and echo's that from the endpoint.
 *
 * Add a second endpoint that does the same but that uses a post.
 * This time use a single input json field 'message'.
 *
 */

public class SpringController {

    public static final String ECHO_ENDPOINT = "/echo";

}
