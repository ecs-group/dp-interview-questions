# Back End Design & Development task: Random Thing Chooser

## Background

Python's standard library already provides a function [random.choice](https://docs.python.org/3/library/random.html#functions-for-sequences) which picks a single object from a sequence with flat probability, that is to say no item is more likely to be chosen than any other item.

In this exercise you are asked to create an alternative version of this function in which the probability of an item being chosen can be set per item.

The input to this function will be a sequence containing a number of objects, one of which is to be chosen, and a sequence of probabilities. 

This function should be stateless. Each execution is independent and has no effect on any other execution. 

# Implementation guidance

The first part of this assignment is to design an interface. You are free to implement this as a function, class or in whatever way makes most sense to you.

For example, it could be a function:

```python
chosen_thing = random_thing_chooser(probabilities=[0.5, 0.1, 0.4], items[A, B, C])
```

Or it could be something else:

```python
chooser = RandomThingChooser(probabilities=[0.5, 0.1, 0.4], items[A, B, C])
thing0 = next(chooser)
thing1 = next(chooser)
```

# Example use

The function is given three objects, A, B and C. The probabilities are 0.5, 0.1 and 0.4. If this function is run 100 times, the expectation would be that A would be returned approximately 50 times, B returned approximately 10 times and the with the remainder C.

## Objective

Create this function using test driven development which has this behavior. You are free to use any Python testing framework.

You may be aware of a library function which already has this exact behavior. The goal of the exercise is to re-create it using more basic Python components.

