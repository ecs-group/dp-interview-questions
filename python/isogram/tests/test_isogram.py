import pytest

from isogram import is_isogram

SINGLE_ISOGRAMS = ["abc", "def", "hij"]


def test_empty():
    assert not is_isogram("")


@pytest.mark.parametrize("word", SINGLE_ISOGRAMS)
def test_single(word):
    assert is_isogram(word)
