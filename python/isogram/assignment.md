# Isogram

Determine if a word or phrase is an [isogram](https://en.wikipedia.org/wiki/Heterogram_(literature)#Isograms).

An isogram (also known as a "nonpattern word") is a word or phrase without a repeating letter, however spaces and hyphens are allowed to appear multiple times.

Examples of isograms:

    lumberjacks
    background
    downstream
    six-year-old

These words are isograms because in each word there is exactly one instance of each letter.

The word `isograms`, however, is not an isogram, because there are 2 instances of the letter `s` but one instance of every other letter.

## Assignment

1. Get this project working so that you can run the script `find_isograms.sh` without a problem.

2. Modify the program so that the input file and output file can be set on the command line. By default it should still read from stdin and write to stdout.

3. Modify the program so that you can specify whether you want to find all isograms, or just isograms of the order n. An order 2 isogram is one where every letter is repeated twice, for example "tutu"
