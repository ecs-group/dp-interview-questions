import sys

from isogram import is_isogram


def main():
    for line in sys.stdin:
        word = line.strip()
        if is_isogram(word):
            print(word)
    else:
        sys.stderr.write("No words found.")


if __name__ == "__main__":
    main()
